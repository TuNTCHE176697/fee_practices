function item(id, quantity, price, discount) {
    this.id = id;
    this.quantity = quantity;
    this.price = price;
    this.discount = discount;
    this.tax = function() {
        return Math.ceil(this.quantity * this.price * 0.125);
    };
    this.total = function() {
        return this.quantity * this.price - this.discount + this.tax();
    };
}

let item1 = new item(1, 1, 120, 25);
let item2 = new item(2, 1, 7, 0);
let item3 = new item(3, 1, 120, 25);

let products = [item1, item2, item3];


function Decrease(id) {
    for(let i = 0; i < products.length;i++){
        if(products[i].id == id) {
            if(products[i].quantity > 1){
                products[i].quantity--;
            }
            else{
                products[i].quantity = 1;
            }
            document.getElementById('item'+id).value = products[i].quantity;
            document.getElementById('tax'+id).innerText = "$" + products[i].tax().toFixed(2);
            document.getElementById('total'+id).innerText = "$" + products[i].total().toFixed(2);
        }
    }
    CalculateTotal();
}

function Encrease(id) {
    for(let i = 0; i < products.length;i++){
        if(products[i].id == id) {
            products[i].quantity ++;
            document.getElementById('item'+id).value = products[i].quantity;
            document.getElementById('tax'+id).innerText = "$" + products[i].tax().toFixed(2);
            document.getElementById('total'+id).innerText = "$" + products[i].total().toFixed(2);
        }
    }
    CalculateTotal();
}

function CalculateTotal(){
    let price = 0;
    let discount = 0;
    let tax = 0;
    for(let i = 0; i < products.length;i++){
        price += products[i].total();
        discount += products[i].discount;
        tax += products[i].tax();
    }
    document.getElementById('totalprice').innerText = "$" + price.toFixed(2);
    document.getElementById('totaldiscount').innerText = "$" + discount.toFixed(2);
    document.getElementById('totaltax').innerText = "$" + tax.toFixed(2);
}

function Remove(id){
    for(let i = 0;i < products.length;i++){
        if(products[i].id == id) {
            products.splice(i, 1);
            document.getElementById('row'+id).style.display="none";
        }
    }
    CalculateTotal();
}
