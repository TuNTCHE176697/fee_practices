$(function() {
    function item(id, quantity, price, discount) {
        this.id = id;
        this.quantity = quantity;
        this.price = price;
        this.discount = discount;
        this.tax = function() {
            return Math.ceil(this.quantity * this.price * 0.125);
        };
        this.total = function() {
            return this.quantity * this.price - this.discount + this.tax();
        };
    }
    
    let item1 = new item(1, 1, 120, 25);
    let item2 = new item(2, 1, 7, 0);
    let item3 = new item(3, 1, 120, 25);
    
    let products = [item1, item2, item3];
    
    
 
    function Decrease(id) {
        products.forEach(function(product) {
            if (product.id == id) {
                product.quantity = Math.max(1, product.quantity - 1);
                updateProductElements(id, product);
            }
        });
        CalculateTotal();
    }

    function Encrease(id) {
        products.forEach(function(product) {
            if (product.id == id) {
                product.quantity++;
                updateProductElements(id, product);
            }
        });
        CalculateTotal();
    }

    function updateProductElements(id, product) {
        let itemElement = $('#item' + id);
        let taxElement = $('#tax' + id);
        let totalElement = $('#total' + id);

        itemElement.val(product.quantity);
        taxElement.text("$" + product.tax().toFixed(2));
        totalElement.text("$" + product.total().toFixed(2));
    }

    function CalculateTotal() {
        let price = 0;
        let discount = 0;
        let tax = 0;

        products.forEach(function(product) {
            price += product.total();
            discount += product.discount;
            tax += product.tax();
        });

        $('#totalprice').text("$" + price.toFixed(2));
        $('#totaldiscount').text("$" + discount.toFixed(2));
        $('#totaltax').text("$" + tax.toFixed(2));
    }

    function Remove(id) {
        products.forEach(function(product, index) {
            if (product.id == id) {
                products.splice(index, 1);
                $('#row' + id).css('display', 'none');
            }
        });
        CalculateTotal();
    }

    $('.minus').on('click', function() {
        let id = $(this).closest('.amount').data('id');
        Decrease(id);
    });

    $('.add').on('click', function() {
        let id = $(this).closest('.amount').data('id');
        Encrease(id);
    });
    $('.remove').on('click', function() {
        let id = $(this).closest('.amount').data('id');
        Remove(id);
    });
});
