function item(id, quantity, price) {
    this.id = id;
    this.quantity = quantity;
    this.price = price;
    this.total = function() {
        return this.quantity * this.price;
    };
}

let item1 = new item(1, 1, 294.50);
let item2 = new item(2, 1, 250.00);

let products = [item1, item2];


function Decrease(id) {
    for(let i = 0; i < products.length;i++){
        if(products[i].id == id) {
            if(products[i].quantity > 1){
                products[i].quantity--;
            }
            else{
                products[i].quantity = 1;
            }
            document.getElementById('quantity'+id).value = products[i].quantity;
            document.getElementById('total'+id).innerText = "$" + products[i].total().toFixed(2);
        }
    }
    CalculateTotal();
}

function Encrease(id) {
    for(let i = 0; i < products.length;i++){
        if(products[i].id == id) {
            products[i].quantity ++;
            document.getElementById('quantity'+id).value = products[i].quantity;
            document.getElementById('total'+id).innerText = "$" + products[i].total().toFixed(2);
        }
    }
    CalculateTotal();
}

function CalculateTotal(){
    let total = 0;
    
    for(let i = 0; i < products.length;i++){
        total += products[i].total();
        
    }
    document.getElementById('subtotal').innerText = "$" + total.toFixed(2);
    document.getElementById('total').innerText = "$" + total.toFixed(2);
}


