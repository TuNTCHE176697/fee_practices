$(document).ready(function () {
    function resetForm() {
        $('#form')[0].reset();
    }

    function getValue(value) {
        return $('#' + value).val().trim();
    }

    function showError(key, message) {
        $('#' + key).html(message);
    }

    function validateForm() {
        var check = true;

        var firstName = getValue('firstName');
        showError('error1',"");
        if (firstName === '') {
            check = false;
            showError('error1', 'The First Name should not be blank');
        }

        var lastName = getValue('lastName');
        showError('error2',"");
        if (lastName === '') {
            check = false;
            showError('error2', 'The Last Name should not be blank');
        }

        var email = getValue('email').toLowerCase();
        var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        showError('error3',"");
        if (email === '') {
            check = false;
            showError('error3', 'The Email should not be blank');
        } else if (!email.match(validRegex)) {
            check = false;
            showError('error3', 'Please input your correct email address');
        }

        var phone = getValue('phone');
        var validPhone = /^\d{3}-\d{3}-\d{7}$/;
        showError('error4',"");
        if (!phone.match(validPhone)) {
            check = false;
            showError('error4', 'Please input your correct phone number');
        }

        var country = getValue('country');
        showError('error5',"");
        if (country === 'default') {
            check = false;
            showError('error5', 'Please select your country');
        }

        var position = getValue('position');
        showError('error6',"");
        if (position === 'default') {
            check = false;
            showError('error6', 'Please select the position');
        }
        return check;
    }

    $('.reset').on('click', function() {
        resetForm();
    });

    $('.submit').on('click', function(e) {
        validateForm();
        e.preventDefault();
    });
});