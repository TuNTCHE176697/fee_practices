var countries = new Array();
countries[0] = "Vietnam";
countries[1] = "Singapore";
countries[2] = "Malaysia";
countries[3] = "Indonesia";
countries[4] = "Philippine";
countries[5] = "Lao";
countries[6] = "Cambodia";

var position = new Array();
position[0] = "Operator";
position[1] = "Manager";
position[2] = "Developer";
position[3] = "Designer";
position[4] = "Tester";

var selectCountry = document.getElementById("country");
var selectPosition = document.getElementById("position");

for (var i = 0; i < countries.length; i++) {
    var option = document.createElement("option");
    option.text = countries[i];
    option.value = countries[i];
    selectCountry.add(option);
}

for (var i = 0; i < position.length; i++) {
    var option = document.createElement("option");
    option.text = position[i];
    option.value = position[i];
    selectPosition.add(option);
}