$(document).ready(function () {
$("#ViewContentLink").on("click", function (e) {
    e.preventDefault();

    $("#loadContainer").html("Loading");

    setTimeout(function () {
      $.ajax({
        url: "/Assignment.CMS/viewcontent.html",
        type: "GET",
        success: function (data) {
          var targetContent = $(data).find("#targetContainer").html();

          if (targetContent) {
            $("#loadContainer").html(targetContent);
          } else {
            $("#loadContainer").html("Error loading content");
          }
        },
        error: function () {
          $("#loadContainer").html("Error loading content");
        },
      });
    }, 5000);
  });
});
