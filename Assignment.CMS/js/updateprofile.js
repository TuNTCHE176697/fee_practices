document.addEventListener("DOMContentLoaded", function () {
  var emailCookie = getCookie("email");
  var firstnameCookie = getCookie("firstname");
  var lastnameCookie = getCookie("lastname");
  var phoneCookie = getCookie("phone");
  var descriptionCookie = getCookie("description");

  if (emailCookie !== "") {
    document.getElementById("email").value = emailCookie;
  }

  if (firstnameCookie !== "") {
    document.getElementById("firstname").value =firstnameCookie;
  }
  if (lastnameCookie !== "") {
    document.getElementById("lastname").value = lastnameCookie;
  }
  if (phoneCookie !== "") {
    document.getElementById("phone").value = phoneCookie;
  }
  if (descriptionCookie !== "") {
    document.getElementById("description").value = descriptionCookie;
  }

  function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == " ") c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return "";
  }
});


function validateEdit(){
    var check = true;

    var firstname = getValue('firstname');
    showError('error1',"");
    if(firstname == '')
    {
        check = false;
        showError('error1',"First Name is required");
    }
    else if(firstname.length < 3 || firstname.length > 30)
    {
        check = false;
        showError('error1',"First Name must be between 3 and 30 characters");
    }

    var lastname = getValue('lastname');
    showError('error2',"");
    if(lastname == '')
    {
        check = false;
        showError('error2',"Last Name is required");
    }
    else if(lastname.length < 3 || lastname.length > 30)
    {
        check = false;
        showError('error2',"Last Name must be between 3 and 30 characters");
    }

    var phone = getValue('phone');
    showError('error3',"");
    if(phone == '')
    {
        check = false;
        showError('error3',"Phone Number is required");
    }
    else if(phone.length < 9 || phone.length > 13)
    {
        check = false;
        showError('error3',"Phone Number must be between 9 and 13 numbers");
    }

    var description = getValue('description');
    showError('error4',"");
    if(description.length > 200)
    {
        check = false;
        showError('error4',"Description must have smaller than 200 characters");
    }

    document.cookie = "firstname="+firstname;
    document.cookie = "lastname="+lastname;
    document.cookie = "phone="+phone;
    document.cookie = "description="+description;

    reloadPageWithAjax();
    return check;
}
function getValue(value){
    return document.getElementById(value).value.trim();
}

function showError(key, message){
    document.getElementById(key).innerHTML = message;
}

function reloadPageWithAjax() {
    $.ajax({
        url: '/Assignment.CMS/editprofile.html',  
        method: 'GET',
        success: function(data) {
            console.log(data);
            onDOMContentLoaded();
        },
        error: function(error) {
            console.error('Error during AJAX request:', error);
        }
    });
}

