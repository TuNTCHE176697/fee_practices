function getValue(value){
    return document.getElementById(value).value.trim();
}

function showError(key, message){
    document.getElementById(key).innerHTML = message;
}

function validateLogin(){
    var check = true;
    var email = getValue('email');
    showError('error1',"");
    if(email == '')
    {
        check = false;
        showError('error1',"Email is required");
    }
    else if(email.length < 5 || email.length > 50)
    {
        check = false;
        showError('error1',"Email must be between 5 and 50 characters");
    }

    var password = getValue('password');
    showError('error2',"");
    if(password == '')
    {
        check = false;
        showError('error2',"Password is required");
    }
    else if(password.length < 8 || password.length > 30)
    {
        check = false;
        showError('error2',"Password must be between 8 and 30 characters");
    }

    setCookie('email',email,60);
    setCookie('password',password,60);
    return check;
}

function validateRegister(){
    var check = true;

    var username = getValue('username');
    showError('error1',"");
    if(username == '')
    {
        check = false;
        showError('error1',"Username is required");
    }
    else if(username.length < 3 || username.length > 30)
    {
        check = false;
        showError('error1',"Username must be between 3 and 30 characters");
    }

    var email = getValue('email');
    showError('error2',"");
    if(email == '')
    {
        check = false;
        showError('error2',"Email is required");
    }
    else if(email.length < 5 || email.length > 50)
    {
        check = false;
        showError('error2',"Email must be between 5 and 50 characters");
    }

    var password = getValue('password');
    showError('error3',"");
    if(password == '')
    {
        check = false;
        showError('error3',"Password is required");
    }
    else if(password.length < 8 || password.length > 30)
    {
        check = false;
        showError('error3',"Password must be between 8 and 30 characters");
    }

    var repassword = getValue('repassword');
    showError('error4',"");
    if(repassword == '')
    {
        check = false;
        showError('error4',"Re-password is required");
    }
    else if(repassword.length < 8 || repassword.length > 30)
    {
        check = false;
        showError('error4',"Re-password must be between 8 and 30 characters");
    }
    else if(repassword != password)
    {
        check = false;
        showError('error4',"Re-password must be the same as password");
    }

    return check;
}



function validateContent(){
    var check = true;

    var title = getValue('title');
    showError('error1',"");
    if(title == '')
    {
        check = false;
        showError('error1',"Title is required");
    }
    else if(title.length < 10 || title.length > 200)
    {
        check = false;
        showError('error1',"Title must be between 10 and 200 characters");
    }

    var brief = getValue('brief');
    showError('error2',"");
    if(brief == '')
    {
        check = false;
        showError('error2',"Brief is required");
    }
    else if(brief.length < 30 || brief.length > 150)
    {
        check = false;
        showError('error2',"Brief must be between 30 and 150 characters");
    }

    var content = getValue('content');
    showError('error3',"");
    if(content == '')
    {
        check = false;
        showError('error3',"Content is required");
    }
    else if(content.length < 50 || content.length > 1000)
    {
        check = false;
        showError('error3',"Content must be between 50 and 1000 characters");
    }

    
    return check;
}


function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires;
}